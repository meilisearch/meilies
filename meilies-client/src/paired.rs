use std::net::SocketAddr;
use std::{fmt, io};

use futures::{Future, Sink, Stream};
use log::warn;
use meilies::reqresp::{Event, Response, ResponseMsgError};
use meilies::reqresp::{Request, RequestMsgError};
use meilies::stream::{EventData, EventName, EventNumber, EventStats, Stream as MeiliesStream, StreamName};
use tokio_retry::Retry;

use super::{connect, SteelConnection};
use crate::steel_connection::retry_strategy;

/// Open a framed paired connection with a server.
pub fn paired_connect(
    addr: SocketAddr,
) -> impl Future<Item = PairedConnection, Error = tokio_retry::Error<io::Error>> {
    PairedConnection::connect(addr)
}

/// A paired connection returns a response to each message send, it is sequential.
/// This connection is used to publish events to streams.
pub struct PairedConnection {
    connection: SteelConnection,
}

#[derive(Debug)]
pub enum PairedConnectionError {
    ServerSide(String),
    ConnectionClosed,
    RequestMsgError(RequestMsgError),
    ResponseMsgError(ResponseMsgError),
    InvalidServerResponse(Response),
}

impl fmt::Display for PairedConnectionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use PairedConnectionError::*;

        match self {
            ServerSide(error) => write!(f, "server side error: {}", error),
            ConnectionClosed => write!(f, "connection closed"),
            RequestMsgError(error) => write!(f, "invalid Request: {}", error),
            ResponseMsgError(error) => write!(f, "invalid Response received: {}", error),
            InvalidServerResponse(response) => {
                write!(f, "invalid server response received: {:?}", response)
            }
        }
    }
}

impl std::error::Error for PairedConnectionError {}

impl PairedConnection {
    /// Open a framed paired connection with a server.
    pub fn connect(
        addr: SocketAddr,
    ) -> impl Future<Item = PairedConnection, Error = tokio_retry::Error<io::Error>> {
        Retry::spawn(retry_strategy(), move || {
            warn!("Connecting to {}", addr);
            connect(&addr).map(move |connection| {
                let connection = SteelConnection::new(addr, connection);
                PairedConnection { connection }
            })
        })
    }

    /// Request the last snapshot for a stream
    ///
    /// Returns `None` if the stream does not contain any snapshot.
    pub fn get_events(
        self,
        stream: MeiliesStream,
    ) -> impl Future<Item = (Vec<Event>, PairedConnection), Error = PairedConnectionError> {
        use PairedConnectionError::*;

        let command = Request::GetEvents { stream };

        self.connection
            .send(command)
            .map_err(RequestMsgError)
            .and_then(|framed| framed.into_future().map_err(|(e, _)| ResponseMsgError(e)))
            .and_then(|(first, connection)| match first.ok_or(ConnectionClosed)? {
                Ok(Response::Events { events }) => Ok((events, PairedConnection { connection })),
                Ok(response) => Err(InvalidServerResponse(response)),
                Err(error) => Err(ServerSide(error)),
            })
    }

    pub fn remove_events(
        self,
        stream: MeiliesStream,
    ) -> impl Future<Item = PairedConnection, Error = PairedConnectionError> {
        use PairedConnectionError::*;

        let command = Request::RemoveEvents { stream };

        self.connection
            .send(command)
            .map_err(RequestMsgError)
            .and_then(|framed| framed.into_future().map_err(|(e, _)| ResponseMsgError(e)))
            .and_then(|(first, connection)| match first.ok_or(ConnectionClosed)? {
                Ok(Response::Ok { }) => Ok(PairedConnection { connection }),
                Ok(response) => Err(InvalidServerResponse(response)),
                Err(error) => Err(ServerSide(error)),
            })
    }

    pub fn get_events_stats(
        self,
        stream: MeiliesStream,
    ) -> impl Future<Item = (Vec<EventStats>, PairedConnection), Error = PairedConnectionError> {
        use PairedConnectionError::*;

        let command = Request::GetEventsStats { stream };

        self.connection
            .send(command)
            .map_err(RequestMsgError)
            .and_then(|framed| framed.into_future().map_err(|(e, _)| ResponseMsgError(e)))
            .and_then(|(first, connection)| match first.ok_or(ConnectionClosed)? {
                Ok(Response::EventsStats { stats }) => Ok((stats, PairedConnection { connection })),
                Ok(response) => Err(InvalidServerResponse(response)),
                Err(error) => Err(ServerSide(error)),
            })
    }

    /// Request the last snapshot for a stream
    ///
    /// Returns `None` if the stream does not contain any snapshot.
    pub fn get_snapshot(
        self,
        stream: StreamName,
    ) -> impl Future<
        Item = (StreamName, EventNumber, EventData, PairedConnection),
        Error = PairedConnectionError,
    > {
        use PairedConnectionError::*;

        let command = Request::GetSnapshot { stream };

        self.connection
            .send(command)
            .map_err(RequestMsgError)
            .and_then(|framed| framed.into_future().map_err(|(e, _)| ResponseMsgError(e)))
            .and_then(|(first, connection)| match first.ok_or(ConnectionClosed)? {
                Ok(Response::Snapshot {
                    stream,
                    number,
                    data,
                }) => Ok((stream, number, data, PairedConnection { connection })),
                Ok(response) => Err(InvalidServerResponse(response)),
                Err(error) => Err(ServerSide(error)),
            })
    }

    /// Publish an event to a stream, specifying the event name and data.
    pub fn publish(
        self,
        stream: StreamName,
        event_name: EventName,
        event_data: EventData,
    ) -> impl Future<Item = PairedConnection, Error = PairedConnectionError> {
        use PairedConnectionError::*;

        let command = Request::Publish {
            stream,
            event_name,
            event_data,
        };

        self.connection
            .send(command)
            .map_err(RequestMsgError)
            .and_then(|framed| framed.into_future().map_err(|(e, _)| ResponseMsgError(e)))
            .and_then(|(first, connection)| match first.ok_or(ConnectionClosed)? {
                Ok(Response::Ok) => Ok(PairedConnection { connection }),
                Ok(response) => Err(InvalidServerResponse(response)),
                Err(error) => Err(ServerSide(error)),
            })
    }

    /// Publish an event to a stream, specifying the event name and data.
    pub fn propose_snapshot(
        self,
        stream: StreamName,
        number: EventNumber,
    ) -> impl Future<Item = (StreamName, EventNumber, PairedConnection), Error = PairedConnectionError>
    {
        use PairedConnectionError::*;

        let command = Request::ProposeSnapshot { stream, number };

        self.connection
            .send(command)
            .map_err(RequestMsgError)
            .and_then(|framed| framed.into_future().map_err(|(e, _)| ResponseMsgError(e)))
            .and_then(|(first, connection)| match first.ok_or(ConnectionClosed)? {
                Ok(Response::SnapshotAccepted { stream, number }) => {
                    Ok((stream, number, PairedConnection { connection }))
                }
                Ok(response) => Err(InvalidServerResponse(response)),
                Err(error) => Err(ServerSide(error)),
            })
    }

    /// Publish a snapshot to a stream, specifying the event number and data.
    pub fn publish_snapshot(
        self,
        stream: StreamName,
        number: EventNumber,
        data: EventData,
    ) -> impl Future<Item = (StreamName, EventNumber, PairedConnection), Error = PairedConnectionError>
    {
        use PairedConnectionError::*;

        let command = Request::PublishSnapshot {
            stream,
            number,
            data,
        };

        self.connection
            .send(command)
            .map_err(RequestMsgError)
            .and_then(|framed| framed.into_future().map_err(|(e, _)| ResponseMsgError(e)))
            .and_then(|(first, connection)| match first.ok_or(ConnectionClosed)? {
                Ok(Response::SnapshotAccepted { stream, number }) => {
                    Ok((stream, number, PairedConnection { connection }))
                }
                Ok(response) => Err(InvalidServerResponse(response)),
                Err(error) => Err(ServerSide(error)),
            })
    }

    /// Request the last event number that the stream is at.
    ///
    /// Returns `None` if the stream does not contain any event.
    pub fn last_event_number(
        self,
        stream: StreamName,
    ) -> impl Future<
        Item = (StreamName, Option<EventNumber>, PairedConnection),
        Error = PairedConnectionError,
    > {
        use PairedConnectionError::*;

        let command = Request::LastEventNumber { stream };

        self.connection
            .send(command)
            .map_err(RequestMsgError)
            .and_then(|framed| framed.into_future().map_err(|(e, _)| ResponseMsgError(e)))
            .and_then(|(first, connection)| match first.ok_or(ConnectionClosed)? {
                Ok(Response::LastEventNumber { stream, number }) => {
                    Ok((stream, number, PairedConnection { connection }))
                }
                Ok(response) => Err(InvalidServerResponse(response)),
                Err(error) => Err(ServerSide(error)),
            })
    }

    /// Request the list of stream names
    ///
    /// Returns an empty Vec if the database does not contain any stream.
    pub fn stream_names(
        self,
    ) -> impl Future<Item = (Vec<StreamName>, PairedConnection), Error = PairedConnectionError>
    {
        use PairedConnectionError::*;

        let command = Request::StreamNames;

        self.connection
            .send(command)
            .map_err(RequestMsgError)
            .and_then(|framed| framed.into_future().map_err(|(e, _)| ResponseMsgError(e)))
            .and_then(|(first, connection)| match first.ok_or(ConnectionClosed)? {
                Ok(Response::StreamNames { streams }) => {
                    Ok((streams, PairedConnection { connection }))
                }
                Ok(response) => Err(InvalidServerResponse(response)),
                Err(error) => Err(ServerSide(error)),
            })
    }
}
