use crate::resp::{FromResp, RespValue};
use crate::stream::{EventData, EventName, EventNumber, InfoMessage, StreamName, EventStats};
use std::fmt;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Event {
    pub stream: StreamName,
    pub number: EventNumber,
    pub event_name: EventName,
    pub event_data: EventData,
}

impl Into<RespValue> for Event {
    fn into(self) -> RespValue {
        RespValue::Array(vec![
            RespValue::string(self.stream),
            RespValue::Integer(self.number.0 as i64),
            RespValue::string(self.event_name),
            RespValue::bulk_string(self.event_data.0),
        ])
    }
}

impl FromResp for Event {
    type Error = RespResponseConvertError;

    fn from_resp(value: RespValue) -> Result<Self, Self::Error> {
        use RespResponseConvertError::*;

        let mut iter = match value {
            RespValue::Array(array) => array.into_iter(),
            _otherwise => return Err(InvalidResponseRespType),
        };

        let stream = iter
            .next()
            .map(StreamName::from_resp)
            .ok_or(MissingArgument)?
            .map_err(|_| InvalidArgumentRespType)?;

        let number = iter
            .next()
            .map(EventNumber::from_resp)
            .ok_or(MissingArgument)?
            .map_err(|_| InvalidArgumentRespType)?;

        let event_name = iter
            .next()
            .map(EventName::from_resp)
            .ok_or(MissingArgument)?
            .map_err(|_| InvalidArgumentRespType)?;

        let event_data = iter
            .next()
            .map(EventData::from_resp)
            .ok_or(MissingArgument)?
            .map_err(|_| InvalidArgumentRespType)?;

        if iter.next().is_some() {
            return Err(TooManyArguments);
        }

        Ok(Event {
            stream,
            number,
            event_name,
            event_data,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Response {
    Ok,
    Subscribed {
        stream: StreamName,
    },
    Event {
        stream: StreamName,
        number: EventNumber,
        event_name: EventName,
        event_data: EventData,
    },
    Events {
        events: Vec<Event>,
    },
    EventsStats {
        stats: Vec<EventStats>,
    },
    Snapshot {
        stream: StreamName,
        number: EventNumber,
        data: EventData,
    },
    LastEventNumber {
        stream: StreamName,
        number: Option<EventNumber>,
    },
    StreamNames {
        streams: Vec<StreamName>,
    },
    Info {
        stream: StreamName,
        message: InfoMessage,
    },
    NoSnapshot {
        stream: StreamName,
    },
    NoEvent {
        stream: StreamName,
    },
    NoEventStats {
        stream: StreamName,
    },
    StartFollowing {
        stream: StreamName,
    },
    SnapshotAccepted {
        stream: StreamName,
        number: EventNumber,
    },
    SnapshotRejected {
        stream: StreamName,
        number: EventNumber,
    },
}

impl Into<RespValue> for Response {
    fn into(self) -> RespValue {
        match self {
            Response::Ok => RespValue::string("OK"),
            Response::Subscribed { stream } => RespValue::Array(vec![
                RespValue::string("subscribed"),
                RespValue::string(stream),
            ]),
            Response::Event {
                stream,
                number,
                event_name,
                event_data,
            } => RespValue::Array(vec![
                RespValue::string("event"),
                RespValue::string(stream),
                RespValue::Integer(number.0 as i64),
                RespValue::string(event_name),
                RespValue::bulk_string(event_data.0),
            ]),
            Response::Events { events } => {
                let command = RespValue::string("events");
                let events = events.into_iter().map(Into::into);
                let args = Some(command).into_iter().chain(events).collect();
                RespValue::Array(args)
            }
            Response::EventsStats { stats } => {
                let command = RespValue::string("events-stats");
                let events = stats.into_iter().map(Into::into);
                let args = Some(command).into_iter().chain(events).collect();
                RespValue::Array(args)
            }
            Response::Snapshot {
                stream,
                number,
                data,
            } => RespValue::Array(vec![
                RespValue::string("snapshot"),
                RespValue::string(stream),
                RespValue::Integer(number.0 as i64),
                RespValue::bulk_string(data.0),
            ]),
            Response::LastEventNumber { stream, number } => {
                let number = match number {
                    Some(number) => RespValue::Integer(number.0 as i64),
                    None => RespValue::Nil,
                };

                RespValue::Array(vec![
                    RespValue::string("last-event-number"),
                    RespValue::string(stream),
                    number,
                ])
            }
            Response::StreamNames { streams } => {
                let command = RespValue::string("stream-names");
                let streams = streams
                    .into_iter()
                    .map(|s| RespValue::SimpleString(s.into_inner()));
                let args = Some(command).into_iter().chain(streams).collect();
                RespValue::Array(args)
            }
            Response::Info { stream, message } => RespValue::Array(vec![
                RespValue::string("info"),
                RespValue::string(stream),
                RespValue::string(message),
            ]),
            Response::NoSnapshot { stream } => RespValue::Array(vec![
                RespValue::string("no-snapshot"),
                RespValue::string(stream),
            ]),
            Response::NoEvent { stream } => RespValue::Array(vec![
                RespValue::string("no-event"),
                RespValue::string(stream),
            ]),
            Response::NoEventStats { stream } => RespValue::Array(vec![
                RespValue::string("no-event-stats"),
                RespValue::string(stream),
            ]),
            Response::StartFollowing { stream } => RespValue::Array(vec![
                RespValue::string("start-following"),
                RespValue::string(stream),
            ]),
            Response::SnapshotAccepted { stream, number } => RespValue::Array(vec![
                RespValue::string("snapshot-accepted"),
                RespValue::string(stream),
                RespValue::Integer(number.0 as i64),
            ]),
            Response::SnapshotRejected { stream, number } => RespValue::Array(vec![
                RespValue::string("snapshot-rejected"),
                RespValue::string(stream),
                RespValue::Integer(number.0 as i64),
            ]),
        }
    }
}

#[derive(Debug)]
pub enum RespResponseConvertError {
    InvalidResponseRespType,
    InvalidArgumentRespType,
    MissingTypeName,
    UnknownTypeName,
    MissingArgument,
    TooManyArguments,
}

impl fmt::Display for RespResponseConvertError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use RespResponseConvertError::*;
        match self {
            InvalidResponseRespType => write!(f, "Invalid response resp type"),
            InvalidArgumentRespType => write!(f, "Invalid argument resp type"),
            MissingTypeName => write!(f, "Missing type name"),
            UnknownTypeName => write!(f, "Unknown type name"),
            MissingArgument => write!(f, "Missing argument"),
            TooManyArguments => write!(f, "Too many arguments"),
        }
    }
}

impl std::error::Error for RespResponseConvertError {}

impl FromResp for Response {
    type Error = RespResponseConvertError;

    fn from_resp(value: RespValue) -> Result<Self, Self::Error> {
        use RespResponseConvertError::*;

        let mut iter = match value {
            RespValue::SimpleString(ref text) if text == "OK" => return Ok(Response::Ok),
            RespValue::Array(array) => array.into_iter(),
            _otherwise => return Err(InvalidResponseRespType),
        };

        let response_type = iter
            .next()
            .map(String::from_resp)
            .ok_or(MissingTypeName)?
            .map_err(|_| InvalidArgumentRespType)?;

        match response_type.as_str() {
            "subscribed" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::Subscribed { stream })
            }
            "event" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                let number = iter
                    .next()
                    .map(EventNumber::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                let event_name = iter
                    .next()
                    .map(EventName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                let event_data = iter
                    .next()
                    .map(EventData::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::Event {
                    stream,
                    number,
                    event_name,
                    event_data,
                })
            }
            "events" => {
                let events = iter.map(Event::from_resp).filter_map(Result::ok).collect();

                Ok(Response::Events { events })
            }
            "events-stats" => {
                let stats = iter.map(EventStats::from_resp).filter_map(Result::ok).collect();

                Ok(Response::EventsStats { stats })
            }
            "snapshot" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                let number = iter
                    .next()
                    .map(EventNumber::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                let data = iter
                    .next()
                    .map(EventData::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::Snapshot {
                    stream,
                    number,
                    data,
                })
            }
            "last-event-number" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                let number = iter
                    .next()
                    .map(FromResp::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::LastEventNumber { stream, number })
            }
            "stream-names" => match iter.map(StreamName::from_resp).collect() {
                Ok(streams) => Ok(Response::StreamNames { streams }),
                Err(_) => Err(InvalidArgumentRespType),
            },
            "info" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                let message = iter
                    .next()
                    .map(InfoMessage::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::Info { stream, message })
            }
            "no-snapshot" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::NoSnapshot { stream })
            }
            "no-event" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::NoEvent { stream })
            }
            "no-event-stats" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::NoEventStats { stream })
            }
            "start-following" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::StartFollowing { stream })
            }
            "snapshot-accepted" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                let number = iter
                    .next()
                    .map(EventNumber::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::SnapshotAccepted { stream, number })
            }
            "snapshot-rejected" => {
                let stream = iter
                    .next()
                    .map(StreamName::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                let number = iter
                    .next()
                    .map(EventNumber::from_resp)
                    .ok_or(MissingArgument)?
                    .map_err(|_| InvalidArgumentRespType)?;

                if iter.next().is_some() {
                    return Err(TooManyArguments);
                }

                Ok(Response::SnapshotRejected { stream, number })
            }
            _otherwise => Err(UnknownTypeName),
        }
    }
}
