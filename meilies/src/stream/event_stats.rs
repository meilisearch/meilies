use std::error::Error;
use std::{fmt, str};

use miniserde::{json, Deserialize, Serialize};

use crate::resp::{FromResp, RespValue};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct EventStats {
    pub number: u64,
    pub name: String,
    // time in second
    pub created_at: u64,
    // size in octet
    pub size: u64,
}

impl EventStats {
    pub fn into_bytes(self) -> Vec<u8> {
        json::to_string(&self).into_bytes()
    }

    pub fn from_bytes(value: Vec<u8>) -> Result<Self, Box<Error>> {
        let value = str::from_utf8(&value)?;
        Ok(json::from_str(&value)?)
    }
}

impl fmt::Display for EventStats {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl Into<RespValue> for EventStats {
    fn into(self) -> RespValue {
        RespValue::BulkString(json::to_string(&self).into_bytes())
    }
}

impl FromResp for EventStats {
    type Error = RespEventStatsConvertError;

    fn from_resp(value: RespValue) -> Result<Self, Self::Error> {
        match String::from_resp(value) {
            Ok(string) => json::from_str(&string).map_err(RespEventStatsConvertError::from),
            Err(_) => Err(RespEventStatsConvertError::InvalidRespType),
        }
    }
}

#[derive(Debug)]
pub enum RespEventStatsConvertError {
    InvalidRespType,
    Serde(miniserde::Error),
}

impl fmt::Display for RespEventStatsConvertError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use RespEventStatsConvertError::*;
        match self {
            InvalidRespType => write!(f, "invalid RESP type found, expected String"),
            Serde(e) => write!(f, "Serde error; {}", e),
        }
    }
}

impl std::error::Error for RespEventStatsConvertError {}

impl From<miniserde::Error> for RespEventStatsConvertError {
    fn from(value: miniserde::Error) -> Self {
        RespEventStatsConvertError::Serde(value)
    }
}
