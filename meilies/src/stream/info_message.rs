use std::fmt;
use std::str::FromStr;
use std::string::FromUtf8Error;

use crate::resp::{FromResp, RespStringConvertError, RespValue};

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum InfoMessage {
    ErrorMessage(String),
    WarnMessage(String),
}

impl fmt::Debug for InfoMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "InfoMessage(\"{}\")", self)
    }
}

impl fmt::Display for InfoMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            InfoMessage::ErrorMessage(message) => write!(f, "Error: {}", message),
            InfoMessage::WarnMessage(message) => write!(f, "Warning: {}", message),
        }
    }
}

impl Into<RespValue> for InfoMessage {
    fn into(self) -> RespValue {
        let text = match self {
            InfoMessage::ErrorMessage(message) => format!("Error: {}", message),
            InfoMessage::WarnMessage(message) => format!("Warning: {}", message),
        };

        RespValue::BulkString(text.into_bytes())
    }
}

#[derive(Debug)]
pub enum RespInfoMessageConvertError {
    InvalidRespType,
    InvalidUtf8String(FromUtf8Error),
    InnerStreamConvertError(ParseInfoMessageError),
}

impl fmt::Display for RespInfoMessageConvertError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use RespInfoMessageConvertError::*;
        match self {
            InvalidRespType => write!(f, "invalid RESP type found, expected String"),
            InvalidUtf8String(e) => write!(f, "invalid UTF8 string; {}", e),
            InnerStreamConvertError(e) => write!(f, "inner Stream convert error: {}", e),
        }
    }
}

impl std::error::Error for RespInfoMessageConvertError {}

impl FromResp for InfoMessage {
    type Error = RespInfoMessageConvertError;

    fn from_resp(value: RespValue) -> Result<Self, Self::Error> {
        use RespInfoMessageConvertError::*;
        match String::from_resp(value) {
            Ok(string) => InfoMessage::from_str(&string).map_err(InnerStreamConvertError),
            Err(RespStringConvertError::InvalidRespType) => Err(InvalidRespType),
            Err(RespStringConvertError::InvalidUtf8String(error)) => Err(InvalidUtf8String(error)),
        }
    }
}

impl FromStr for InfoMessage {
    type Err = ParseInfoMessageError;

    fn from_str(s: &str) -> Result<InfoMessage, Self::Err> {
        use ParseInfoMessageError::*;

        let mut split = s.splitn(2, ':');
        match (split.next(), split.next(), split.next()) {
            (Some(attr), Some(message), None) => match attr {
                "Error" => Ok(InfoMessage::ErrorMessage(message.to_string())),
                "Warn" => Ok(InfoMessage::WarnMessage(message.to_string())),
                _ => Err(CaseNotFound(attr.to_string())),
            },
            (_, _, _) => Err(FormatError),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ParseInfoMessageError {
    CaseNotFound(String),
    FormatError,
}

impl fmt::Display for ParseInfoMessageError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ParseInfoMessageError::*;
        match self {
            CaseNotFound(attr) => write!(f, "The case {} is not found for a InfoMessage", attr),
            FormatError => f.write_str("InfoMessage is not properly formatted"),
        }
    }
}

impl std::error::Error for ParseInfoMessageError {}
