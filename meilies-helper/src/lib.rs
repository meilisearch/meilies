#![feature(async_await, generators)]

use std::sync::Arc;
use std::net::SocketAddr;
use std::collections::HashSet;

use log::*;
use futures::future::Future;
use futures::future;
use futures::stream::Stream as _;

use meilies::reqresp::Response;
use meilies::stream::{Stream, StreamName, EventName, EventNumber, EventData, ReadRange};
use meilies_client::{sub_connect, paired_connect, SubController};

#[derive(Debug)]
pub enum Error {
    StreamAlreadySubscribed(String),
    ImpossibleToSubscribeYet
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::StreamAlreadySubscribed(stream) => write!(f, "stream {} already subscribed", stream),
            Error::ImpossibleToSubscribeYet => write!(f, "It is not possible to subscribe at the moment")
        }
    }
}

impl std::error::Error for Error {}

pub trait Subscriber {
    fn on_connect(&self) {
        info!("Subscriber connect");
    }

    fn on_connect_error(&self, error: String) {
        error!("Subscriber connect error; {}", error);
    }

    fn on_disconnect(&self) {
        warn!("Subscriber disconnect");
    }

    fn on_snapshot(&self, _stream: String, _number: u64, _data: Vec<u8>) {
        warn!("Subscriber receive a snapshot but the function is not overrided");
    }

    fn on_subscribe(&self, stream_name: String) {
        info!("Subscriber subscribe to stream {}", stream_name);
    }

    fn on_start_following(&self, stream_name: String) {
        info!("Subscriber start following stream {}", stream_name);
    }

    fn subscribe_from(&self, stream_name: String) -> Option<u64> {
        info!("Subscriber subscribe to stream {} from start", stream_name);
        None
    }

    fn on_event(&self, stream: String, number: u64, event_name: String, event_data: Vec<u8>);

    fn on_error(&self, error: String) {
        error!("Subscriber reveive an error error; {}", error);
    }
}

#[derive(Clone)]
pub struct EventLoop {
    addr: SocketAddr,
    streams: HashSet<String>,
    ctrl: Option<SubController>,
    subscriber: Arc<Subscriber + Sync + Send>,
}

impl EventLoop {
    pub fn new(addr: SocketAddr, subscriber: Arc<Subscriber + Sync + Send>) -> EventLoop {
        EventLoop {
            addr,
            streams: HashSet::new(),
            ctrl: None,
            subscriber
        }
    }

    pub fn connect_to(mut self, stream: String) -> impl Future<Item=(), Error=()> {
        let sub_connect_error = self.subscriber.clone();
        let sub_event_loop = self.subscriber.clone();
        let sub_con_close = self.subscriber.clone();

        sub_connect(self.addr)
            .map_err(move |e| sub_connect_error.on_connect_error(e.to_string()))
            .and_then(move |(mut ctrl, msgs)| {
                sub_event_loop.on_connect();


                self.ctrl = Some(ctrl.clone());

                let start_from = sub_event_loop.subscribe_from(stream.clone()).unwrap_or(0);

                let stream = Stream {
                    name: StreamName::new(stream).unwrap(),
                    range: ReadRange::ReadFrom(start_from),
                };
                ctrl.subscribe_to(stream);
                let sub_event_loop_response = sub_event_loop.clone();
                let sub_event_loop_error = sub_event_loop.clone();
                msgs.for_each(move |msg| {
                    match msg {
                        Ok(response) => self.handle_new_event(response),
                        Err(e) => sub_event_loop_response.on_error(e.to_string()),
                    }
                    future::ok(())
                })
                .map_err(move |e| sub_event_loop_error.on_error(e.to_string()))
            })
            .and_then(move |_| {
                sub_con_close.on_disconnect();
                Err(())
        })
    }

    fn handle_new_event(&mut self, event: Response) {
        match event {
            Response::StartFollowing { stream } => {
                self.subscriber.on_start_following(stream.to_string())
            },
            Response::Subscribed { stream } => {
                self.streams.insert(stream.to_string());
                self.subscriber.on_subscribe(stream.to_string())
            },
            Response::Event { stream, number, event_name, event_data } => {
                self.subscriber.on_event(stream.to_string(), number.0, event_name.to_string(), event_data.0);
            },
            otherwise => {
                self.subscriber.on_error(format!("unreconized response type; {:?}", otherwise))
            }
        }
    }

    pub fn add_subscription(&self, stream: String) -> Result<(), Error> {
        if self.streams.contains(&stream) {
            return Err(Error::StreamAlreadySubscribed(stream));
        }

        let start_from = self.subscriber.subscribe_from(stream.clone()).unwrap_or(0);
        let stream = Stream {
            name: StreamName::new(stream).unwrap(),
            range: ReadRange::ReadFrom(start_from),
        };

        self.ctrl.clone().unwrap().subscribe_to(stream);

        Ok(())
    }

    pub fn push_event(&self, stream: String, name: String, data: Vec<u8>) {
        let fut = paired_connect(self.addr)
            .map_err(|e| error!("{}", e))
            .and_then(|conn| {
                let stream_name = StreamName::new(stream).unwrap();
                let event_name = EventName::new(name).unwrap();
                let event_data = EventData(data);

                conn.publish(stream_name, event_name, event_data)
                    .map_err(|e| error!("{}", e))
            })
            .map(|_conn| debug!("Event sent to the stream"));

        std::thread::spawn(move || {
            tokio::run(fut);
        }).join().unwrap();
    }

    pub fn push_snapshot(&self, stream: String, number: u64, data: Vec<u8>) {
        let fut = paired_connect(self.addr)
            .map_err(|e| error!("{}", e))
            .and_then(move |conn| {
                let stream_name = StreamName::new(stream).unwrap();
                let event_number = EventNumber(number);

                conn.propose_snapshot(stream_name, event_number)
                    .map_err(|e| error!("{}", e))
                    .and_then(move |(stream_name, event_number, conn)| {
                        let event_data = EventData(data);
                        conn.publish_snapshot(stream_name, event_number, event_data)
                            .map_err(|e| error!("{}", e))
                    })
            })
            .map(|_conn| debug!("Event sent to the stream"));

        std::thread::spawn(move || {
            tokio::run(fut);
        }).join().unwrap();
    }

    pub fn request_snapshot(&self, stream: String) {
        let subscriber = self.subscriber.clone();
        let fut = paired_connect(self.addr)
            .map_err(|e| error!("{}", e))
            .and_then(move |conn| {
                let stream_name = StreamName::new(stream).unwrap();

                conn.get_snapshot(stream_name)
                    .map_err(|e| error!("{}", e))
                    .and_then(move |(stream, number, data, _)| {
                        subscriber.on_snapshot(stream.to_string(), number.0, data.0);
                        future::ok(())
                    })
            })
            .map(|_conn| debug!("Event sent to the stream"));

        std::thread::spawn(move || {
            tokio::run(fut);
        }).join().unwrap();
    }
}
